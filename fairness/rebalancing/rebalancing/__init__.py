import random
import pandas as pd
from fairness.rebalancing.pre_processing import *
import itertools
import numpy as np
from datetime import datetime


def rebalance(dataset: pd.DataFrame, protected_attributes: list, output_column: str) -> pd.DataFrame:
    
    numerical_dataset = categorical_to_numeric_converter(dataset)
    final_dataset = fix_protected_attributes(numerical_dataset, protected_attributes)
    combination_attributes = protected_attributes
    combination_attributes.append(output_column)
    
    combination_list = list(map(list, itertools.product([0, 1], repeat=len(combination_attributes))))
    for comb in combination_list:
        print(comb)
    combination_frequency = []
    
    for combination in combination_list:
        combination_frequency.append(len(final_dataset.query(return_query_for_dataframe(combination, combination_attributes))))
    
    print(combination_frequency)
        
    combination_frequency_target = max(combination_frequency)
    
    print(combination_frequency_target)
    
    for index in range(0, len(combination_list)):
        combination = combination_list[index]
        temp_dataset = final_dataset.query(return_query_for_dataframe(combination, combination_attributes))
        if combination_frequency[index] == combination_frequency_target:
            continue
        else:
            for counter in range(0, combination_frequency_target - combination_frequency[index]):
                new_row = {}
                for (attr, value) in zip(combination_attributes, combination):
                    new_row[attr] = value
                for attribute in final_dataset.columns:
                    if attribute not in combination_attributes:
                        min_val, max_val = _frequent_value(attribute, temp_dataset)
                        if is_variable_discrete(temp_dataset, attribute):
                            new_row[attribute] = random.randint(min_val, max_val)
                        else:
                            new_row[attribute] = random.uniform(min_val, max_val)
                
                final_dataset.loc[len(final_dataset)] = new_row
                
    return final_dataset


def return_query_for_dataframe(combination_list: list, combination_attributes: list) -> str:
    query_str = ""
    for (value, attr) in zip(combination_list, combination_attributes):
        query_str += str(attr) + " == " + str(value) + " and "
        
    return query_str[:-5]



def is_variable_continuous(dataset: pd.DataFrame, attribute: str) -> bool:
    """
        This method checks if the attribute is continuous
        Args:
            dataset: the working dataset
            attribute: the attribute to check

        Returns:
            True if the variable is continuous, False otherwise
        """
    for attr in dataset[attribute]:
        if isinstance(attr, float):
            continue
        else:
            return False
    return True


def is_variable_discrete(dataset: pd.DataFrame, attribute: str) -> bool:
    """
    This method checks if the attribute is discrete
    Args:
        dataset: the working dataset
        attribute: the attribute to check

    Returns:
        True if the variable is discrete, False otherwise
    """
    for attr in dataset[attribute]:
        if isinstance(attr, int):
            continue
        else:
            return False
    return True

def _frequent_value(attribute: str, dataset: pd.DataFrame):
    unique_values = dataset[attribute].unique()
    frequency_value = [len(dataset[dataset[attribute] == x]) for x in unique_values]
    less_occurrent_value = unique_values[frequency_value.index(min(frequency_value))]
    most_occurrent_value = unique_values[frequency_value.index(max(frequency_value))]
    
    if most_occurrent_value > less_occurrent_value:
        return less_occurrent_value, most_occurrent_value
    else:
        return most_occurrent_value, less_occurrent_value

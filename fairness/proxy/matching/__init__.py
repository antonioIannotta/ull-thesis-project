import pandas as pd

from fairness.proxy.proxy import proxy_detection
from fairness.fairness_metric import return_disparate_impact


def conscious_fairness_through_unawareness(dataset: pd.DataFrame, protected_attributes: list,
                                           output_column: str) -> pd.DataFrame:
    """
    This method performs the proxy and protected attributes removal
    Args:
        dataset:
        protected_attributes:
        output_column:

    Returns:
        The new fair dataset
    """
    global fixed_dataset
    fairness_eval = _return_fairness_eval(dataset, protected_attributes, output_column)
    attributes_to_remove = []

    while fairness_eval == 'unfair':
        proxy_list = proxy_detection(dataset, protected_attributes, output_column)
        for attribute in proxy_list:
            attributes_to_remove.append(attribute)
        if proxy_list is not []:
            proxy_dataset = dataset.drop(columns=proxy_list)
            if len(fixed_dataset.columns) == len(proxy_dataset.columns):
                fixed_dataset = proxy_dataset.drop(columns=protected_attributes)
                fairness_eval = 'fair'
            else:
                dataset = proxy_dataset
                new_protected_attributes = []
                for protected_attribute in protected_attributes:
                    if protected_attribute in dataset.columns:
                        new_protected_attributes.append(protected_attribute)

                fairness_eval = _return_fairness_eval(dataset, new_protected_attributes, output_column)
        else:
            dataset.drop(protected_attributes, inplace=True)
            fairness_eval = 'fair'

    for protected_attribute in protected_attributes:
        attributes_to_remove.append(protected_attribute)

    return attributes_to_remove


def _return_fairness_eval(dataset: pd.DataFrame, protected_attributes: list, output_column: str):
    for protected_attribute in protected_attributes:
        disparate_impact = return_disparate_impact(dataset, protected_attribute, output_column)
        for val in disparate_impact:
            if val <= 0.8 or val >= 1.25:
                return 'unfair'

    return 'fair'

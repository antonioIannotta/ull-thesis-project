import pandas as pd


def return_disparate_impact(dataset: pd.DataFrame, protected_attribute: str, output_column: str) -> list:
    """
    This method returns a dataframe in which, for each protected attribute is related the correspondent
    Disparate Impact value
    :param dataset: the dataset on which the disparate impact value must be computed
    :param protected_attribute: the protected attribute for which the disparate impact value must be
    computed
    :param output_column: the output column needed to compute the disparate impact value
    :return:
    """
    disparate_impact_array = []
    output_column_values = dataset[protected_attribute].unique()
    for output_value in output_column_values:
        unprivileged_probability = _compute_disparate_impact(dataset, protected_attribute, 0,
                                                             output_column, output_value)

        privileged_probability = _compute_disparate_impact(dataset, protected_attribute, 1,
                                                           output_column, output_value)

        if privileged_probability == 0:
            continue
        else:
            disparate_impact = unprivileged_probability / privileged_probability
            disparate_impact_array.append(disparate_impact)

    return disparate_impact_array


def _compute_disparate_impact(dataset: pd.DataFrame, protected_attribute, protected_attribute_value,
                              output_column, output_value) -> float:
    """
    This method computes the disparate impact value starting from the parameters
    :param dataset: the dataset needed to perform the computation
    :param protected_attribute: the protected attribute on which compute the disparate impact
    :param protected_attribute_value: the value of the protected attribute
    :param output_column: the output of interest
    :param output_value: the value of the output of interest
    :return:
    """
    attribute_columns_data = dataset[dataset[protected_attribute] == protected_attribute_value]

    if attribute_columns_data == 0:
        return 0.7
    return len(attribute_columns_data[attribute_columns_data[output_column] == output_value]) / len(
        attribute_columns_data)

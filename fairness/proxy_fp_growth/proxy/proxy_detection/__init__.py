import pandas as pd
# from apyori import apriori
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import fpgrowth
from mlxtend.frequent_patterns import association_rules


def _return_association_mining_dataset_format(dataset: pd.DataFrame) -> list:
    """
    This method returns the dataset in the format required by the association mining algorithms
    :param dataset: the dataset on which compute the association mining algorithm to find the association rules
    :return: returns the list associated to the dataframe
    """
    records = []
    for i in range(len(dataset)):
        records.append(
            [str(dataset.columns[j] + " = " + str(dataset.values[i, j])) for j in range(len(dataset.columns) - 1)])

    return records


def return_fp_growth_dataframe(dataset: pd.DataFrame) -> pd.DataFrame:
    fp_dataset = _return_association_mining_dataset_format(dataset)
    te = TransactionEncoder()
    te_ary = te.fit(fp_dataset).transform(fp_dataset)
    df = pd.DataFrame(te_ary, columns=te.columns_)

    frequent_item_sets = fpgrowth(df, min_support=0.8, use_colnames=True)

    association_rules_dataset = association_rules(frequent_item_sets, metric="confidence", min_threshold=0.85)
    
    return pd.DataFrame(association_rules_dataset, columns=['antecedents', 'consequents', 'confidence'])

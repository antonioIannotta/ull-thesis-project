from fairness.fairness_metric import return_disparate_impact
from fairness.proxy_fp_growth.pre_processing import *
from fairness.proxy_fp_growth.proxy.proxy_processing import proxy_fixing
import numpy as np


def conscious_fairness_through_unawareness(dataset: pd.DataFrame, protected_attributes: list,
                                           output_column: str) -> pd.DataFrame:
    """
    This method compute the transformation of the dataset. It looks for proxy and, in case these are not founded it
    removes the protected attributes.
    """

    fairness_evaluation = _return_fairness_eval(dataset, protected_attributes, output_column)
    attributes_to_remove = []
    while fairness_evaluation == 'unfair':
        proxy_dataset, proxies = proxy_fixing(dataset, protected_attributes)
        for attribute in proxies:
            attributes_to_remove.append(attribute)
        if len(dataset.columns) == len(dataset.columns):
            dataset = remove_columns_from_dataset(proxy_dataset, protected_attributes)
            fairness_evaluation = 'fair'
        else:
            dataset = proxy_dataset
            new_protected_attributes = []
            for attr in protected_attributes:
                if attr in dataset.columns:
                    new_protected_attributes.append(attr)
            fairness_evaluation = _return_fairness_eval(dataset, new_protected_attributes, output_column)

    for protected_attribute in protected_attributes:
        attributes_to_remove.append(protected_attribute)

    return np.unique(attributes_to_remove)


def _return_fairness_eval(dataset: pd.DataFrame, protected_attributes: list, output_column: str):
    for protected_attribute in protected_attributes:
        disparate_impact = return_disparate_impact(dataset, protected_attribute, output_column)
        for val in disparate_impact:
            if val <= 0.8 or val >= 1.25:
                return 'unfair'

    return 'fair'
